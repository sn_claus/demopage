<?php
/**
 * Запись в цикле (loop.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */ 
?>
<div class="mb-3 <?php if(is_archive()) { echo 'col-12';} else {echo 'col-md-6';} ?>">
      <div class="h-100 row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm position-relative">
        <div class="col p-4 d-flex flex-column position-static">
			<strong class="d-inline-block mb-2  <?php if(in_category(1)) {echo 'text-success';} else {echo 'text-primary';} ?>"><?php the_category(' ') ?></strong>
			<h3 class="mb-0"><?php the_title(); ?></h3>
			<?php if(is_archive('video')): ?>
				<div class="my-3">
					<p class="font-weight-bold mb-1"><span class="text-success">Order: </span><?php echo ((get_post_meta($post->ID, 'video_order', true)) ?  get_post_meta($post->ID, 'video_order', true) : 'none'); ?></p>
					<p class="font-weight-bold mb-1"><span class="text-success">Текстовое поле: </span><?php echo ((get_post_meta($post->ID, 'my_textfield', true)) ?  get_post_meta($post->ID, 'my_textfield', true) : 'none'); ?></p>
				</div>
			<?php endif; ?>
			<div class="mb-1 text-muted"><?php the_time(get_option('date_format')." в ".get_option('time_format')); ?></div>
			<div class="card-text mb-auto"><?php echo wp_trim_words( get_the_content(), 15, '...' ); ?></div>
			<a href="<?php the_permalink(); ?>" class="">Читать</a>
        </div>
        <div class="col-auto d-none d-lg-block">
			<svg class="bd-placeholder-img" width="200" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
        </div>
	</div>
</div>