<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
				<div class="col-12 mb-3 mt-5 no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm position-relative">
					<h1 class="mb-4"><?php the_title(); // заголовок поста ?></h1>
					<div class="meta">
						
						<?php if(is_singular('video')): ?>
							<div class="my-3">
								<p class="font-weight-bold mb-1"><span class="text-success">Order: </span><?php echo ((get_post_meta($post->ID, 'video_order', true)) ?  get_post_meta($post->ID, 'video_order', true) : 'none'); ?></p>
								<p class="font-weight-bold mb-1"><span class="text-success">Текстовое поле: </span><?php echo ((get_post_meta($post->ID, 'my_textfield', true)) ?  get_post_meta($post->ID, 'my_textfield', true) : 'none'); ?></p>
							</div>
						<?php endif; ?>
						<p>Опубликовано: <?php the_time(get_option('date_format')." в ".get_option('time_format')); ?></p> <?php // дата и время создания ?>
						<p>Автор:  <?php the_author_posts_link(); ?></p>
						<?php if(has_category()): ?><p>Категории: <?php the_category(',') ?></p> <?php endif; // ссылки на категории в которых опубликован пост, через зпт ?>
						<?php the_tags('<p>Тэги: ', ',', '</p>'); // ссылки на тэги поста ?>
					</div>
					<?php the_content(); // контент ?>
				</div>
				<?php endwhile; // конец цикла ?>
				<?php previous_post_link('%link', '<- Предыдущий пост: %title', TRUE); // ссылка на предыдущий пост ?> 
				<?php next_post_link('%link', 'Следующий пост: %title ->', TRUE); // ссылка на следующий пост ?> 
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<button class="btn btn-primary float-right" onclick="window.history.back();"><- Вернуться</button>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); // подключаем footer.php ?>
