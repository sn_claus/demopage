<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); // вывод атрибутов языка ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php /* Все скрипты и стили теперь подключаются в functions.php */ ?>

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<?php wp_head(); // необходимо для работы плагинов и функционала ?>
</head>
<body <?php body_class(); // все классы для body ?>>
	<!-- <header>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="navbar navbar-default">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topnav" aria-expanded="false">
								<span class="sr-only">Меню</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="collapse navbar-collapse" id="topnav">
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header> -->
	<header class="blog-header py-3">
		<div class="container">
			<div class="row flex-nowrap justify-content-between align-items-center">
			<div class="col-4 pt-1">
				<a class="text-muted" href="#">Subscribe</a>
			</div>
			<div class="col-4 text-center">
				<a class="blog-header-logo text-dark" href="/">Demo</a>
			</div>
			<div class="col-4 d-flex justify-content-end align-items-center">
				<a class="text-muted" href="#">
				<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img" viewBox="0 0 24 24" focusable="false"><title>Search</title><circle cx="10.5" cy="10.5" r="7.5"></circle><path d="M21 21l-5.2-5.2"></path></svg>
				</a>
				<a class="btn btn-sm btn-outline-secondary disabled" href="#">Sign up</a>
			</div>
			</div>
		</div>
	</header>
	<div class="nav-scroller py-1 mb-2">
		<div class="container">
			<nav class="nav d-flex justify-content-between">
				<!-- <a class="p-2 text-muted" href="#">World</a>
				<a class="p-2 text-muted" href="#">U.S.</a>
				<a class="p-2 text-muted" href="#">Technology</a>
				<a class="p-2 text-muted" href="#">Design</a>
				<a class="p-2 text-muted" href="#">Culture</a>
				<a class="p-2 text-muted" href="#">Business</a>
				<a class="p-2 text-muted" href="#">Politics</a>
				<a class="p-2 text-muted" href="#">Opinion</a>
				<a class="p-2 text-muted" href="#">Science</a>
				<a class="p-2 text-muted" href="#">Health</a>
				<a class="p-2 text-muted" href="#">Style</a>
				<a class="p-2 text-muted" href="#">Travel</a> -->

				<?php
				if (has_nav_menu('top')) {
					wp_nav_menu(['theme_location' => 'top', 'container' => false,'items_wrap' => '%3$s','walker' => new web_walker]);
				}
				?>
			</nav>
		</div>
	</div>