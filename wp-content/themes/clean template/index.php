<?php
/**
 * Главная страница (index.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?> 

<div class="col-12">
	<div class="container">
		<div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
			<div class="col-md-6 px-0">
				<h1 class="display-4 font-italic"><?php // заголовок архивов
					if (is_day()) : printf('Daily Archives: %s', get_the_date()); // если по дням
					elseif (is_month()) : printf('Monthly Archives: %s', get_the_date('F Y')); // если по месяцам
					elseif (is_year()) : printf('Yearly Archives: %s', get_the_date('Y')); // если по годам
					else : 'Archives';
					endif; ?></h1>
				<p class="lead my-3">Demo blog from bootstrap 4</p>
				<p class="lead mb-0"><a href="https://getbootstrap.com/docs/4.3/examples/blog/#" target="_blank" class="text-white font-weight-bold">Ссылка на оригинал...</a></p>
			</div>
		</div>
		<div class="row mb-2">
			<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
				<?php get_template_part('loop'); // для отображения каждой записи берем шаблон loop.php ?>
			<?php endwhile; // конец цикла
			else: echo '<p>Нет записей.</p>'; endif; // если записей нет, напишим "простите" ?>	 
			<?php pagination(); // пагинация, функция нах-ся в function.php
			wp_reset_query();  ?>
		</div>
		<div class="row mt-5">
			<div class="col-12  mb-5">
				<h2 class="border-bottom">А теперь посмотрите наши лучшие видосики</h2>
				<a  class="float-right text-danger font-weight-bold" href="<?php echo get_post_type_archive_link('video') ?>">Перейти к архиву записей Видео</a>
			</div>
			
			<?php  $query_args = array(
				'post_type' => 'video',
				'posts_per_page' => -1,
				'orderby' => 'title',
				'order' => 'ASC'
			);
			$query = new WP_Query( $query_args );
			$uposts = $query->posts;
			foreach($uposts as $post): ?>
				<div class="col-12 mt-2">
					<div class="h-100 row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm position-relative">
						<div class="col p-4 d-flex flex-column position-static">
						<h3 class="mb-0"><?php echo get_the_title($post->ID); ?></h3>
						<div class="my-3">
							<p class="font-weight-bold mb-1"><span class="text-success">Order: </span><?php echo ((get_post_meta($post->ID, 'video_order', true)) ?  get_post_meta($post->ID, 'video_order', true) : 'none'); ?></p>
							<p class="font-weight-bold mb-1"><span class="text-success">Текстовое поле: </span><?php echo ((get_post_meta($post->ID, 'my_textfield', true)) ?  get_post_meta($post->ID, 'my_textfield', true) : 'none'); ?></p>
						</div>
						<div class="mb-1 text-muted"><?php the_time(get_option('date_format', $post->ID)." в ".get_option('time_format', $post->ID)); ?></div>
						<div class="card-text mb-auto"><?php echo $post->post_content; ?></div>
						<a href="<?php the_permalink($post->ID); ?>" class="">Читать</a>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>