<?php
/**
 * Страница архивов записей (archive.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?> 
<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="mb-5 mt-5"><?php echo get_the_archive_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
				<div class="row">
					<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
						<?php get_template_part('loop'); // для отображения каждой записи берем шаблон loop.php ?>
					<?php endwhile; // конец цикла
					else: echo '<p>Нет записей.</p>'; endif; // если записей нет, напишим "простите" ?>	 
					<?php pagination(); // пагинация, функция нах-ся в function.php ?>
				</div>
				<div class="row mb-5">
					<div class="col-12">
						<button class="btn btn-primary float-right" onclick="window.history.back();"><- Вернуться</button>
					</div>
				</div>
			</div>
			<?php get_sidebar(); // подключаем sidebar.php ?>
		</div>
	</div>
</section>
<?php get_footer(); // подключаем footer.php ?>