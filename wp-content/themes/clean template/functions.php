<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */

add_theme_support('title-tag'); // теперь тайтл управляется самим вп

register_nav_menus(array( // Регистрируем 2 меню
	'top' => 'Верхнее', // Верхнее
	'bottom' => 'Внизу' // Внизу
));

add_theme_support('post-thumbnails'); // включаем поддержку миниатюр
set_post_thumbnail_size(250, 150); // задаем размер миниатюрам 250x150
add_image_size('big-thumb', 400, 400, true); // добавляем еще один размер картинкам 400x400 с обрезкой

register_sidebar(array( // регистрируем левую колонку, этот кусок можно повторять для добавления новых областей для виджитов
	'name' => 'Сайдбар', // Название в админке
	'id' => "sidebar", // идентификатор для вызова в шаблонах
	'description' => 'Обычная колонка в сайдбаре', // Описалово в админке
	'before_widget' => '<div id="%1$s" class="widget %2$s">', // разметка до вывода каждого виджета
	'after_widget' => "</div>\n", // разметка после вывода каждого виджета
	'before_title' => '<span class="widgettitle">', //  разметка до вывода заголовка виджета
	'after_title' => "</span>\n", //  разметка после вывода заголовка виджета
));

if (!function_exists('pagination')) { // если ф-я уже есть в дочерней теме - нам не надо её определять
	function pagination() { // функция вывода пагинации
		global $wp_query; // текущая выборка должна быть глобальной
		$big = 999999999; // число для замены
		$links = paginate_links(array( // вывод пагинации с опциями ниже
			'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))), // что заменяем в формате ниже
			'format' => '?paged=%#%', // формат, %#% будет заменено
			'current' => max(1, get_query_var('paged')), // текущая страница, 1, если $_GET['page'] не определено
			'type' => 'array', // нам надо получить массив
			'prev_text'    => 'Назад', // текст назад
	    	'next_text'    => 'Вперед', // текст вперед
			'total' => $wp_query->max_num_pages, // общие кол-во страниц в пагинации
			'show_all'     => false, // не показывать ссылки на все страницы, иначе end_size и mid_size будут проигнорированны
			'end_size'     => 15, //  сколько страниц показать в начале и конце списка (12 ... 4 ... 89)
			'mid_size'     => 15, // сколько страниц показать вокруг текущей страницы (... 123 5 678 ...).
			'add_args'     => false, // массив GET параметров для добавления в ссылку страницы
			'add_fragment' => '',	// строка для добавления в конец ссылки на страницу
			'before_page_number' => '', // строка перед цифрой
			'after_page_number' => '' // строка после цифры
		));
	 	if( is_array( $links ) ) { // если пагинация есть
		    echo '<ul class="pagination">';
		    foreach ( $links as $link ) {
		    	if ( strpos( $link, 'current' ) !== false ) echo "<li class='active'>$link</li>"; // если это активная страница
		        else echo "<li>$link</li>"; 
		    }
		   	echo '</ul>';
		 }
	}
}

add_action('wp_footer', 'add_scripts'); // приклеем ф-ю на добавление скриптов в футер
if (!function_exists('add_scripts')) { // если ф-я уже есть в дочерней теме - нам не надо её определять
	function add_scripts() { // добавление скриптов
	    if(is_admin()) return false; // если мы в админке - ничего не делаем
	    wp_deregister_script('jquery'); // выключаем стандартный jquery
	    wp_enqueue_script('jquery','//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js','','',true); // добавляем свой
	    wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js','','',true); // бутстрап
	    wp_enqueue_script('main', get_template_directory_uri().'/js/main.js','','',true); // и скрипты шаблона
	}
}

add_action('wp_print_styles', 'add_styles'); // приклеем ф-ю на добавление стилей в хедер
if (!function_exists('add_styles')) { // если ф-я уже есть в дочерней теме - нам не надо её определять
	function add_styles() { // добавление стилей
	    if(is_admin()) return false; // если мы в админке - ничего не делаем
	    wp_enqueue_style( 'bs', get_template_directory_uri().'/css/bootstrap.min.css' ); // бутстрап
		wp_enqueue_style( 'main', get_template_directory_uri().'/style.css' ); // основные стили шаблона
	}
}


if (!function_exists('content_class_by_sidebar')) { // если ф-я уже есть в дочерней теме - нам не надо её определять
	function content_class_by_sidebar() { // функция для вывода класса в зависимости от существования виджетов в сайдбаре
		if (is_active_sidebar( 'sidebar' )) { // если есть
			echo 'col-sm-9'; // пишем класс на 80% ширины
		} else { // если нет
			echo 'col-sm-12'; // контент на всю ширину
		}
	}
}

class web_walker extends Walker
{
	public function walk( $elements, $max_depth )
	{
		$list = array ();

		foreach ( $elements as $item )
			$list[] = "<a class='p-2 text-muted' href='$item->url'>$item->title</a>";

		return join( "\n", $list );
	}
}
function register_post_type_spareparts() {
	$spareparts_labels = array(
		'name' => 'Видео',
		'singular_name' => 'Видео',
		'add_new' => 'Добавить новое',
		'add_new_item' => 'Добавить видео',
		'edit_item' => 'Редактировать видео',
		'new_item' => 'Новое видео',
		'search_items' => 'Искать видео',
		'not_found' =>  'Видео не найдены.',
		'not_found_in_trash' => 'В корзине нет видео.',
		'all_items' => 'Все видео',
		'archives' => 'Архив видео',
		'menu_name' => 'Видео',
		'filter_items_list' => 'Фильтровать видео',
		'items_list_navigation' => 'Навигация по видео',
		'items_list' => 'Перечень видео',
		'name_admin_bar' => 'Видео'
	);
	$spareparts_args = array(
		'rewrite' => true,
		'labels' => $spareparts_labels,
		'public' => true,
		'menu_position' => 4,
		'menu_icon' => 'dashicons-media-spreadsheet',
		'hierarchical' => true,
		'supports' => array('title','editor', 'thumbnail', 'page-attributes'),
		'has_archive' => true,
		'show_in_nav_menus' => false,
		'show_ui' => true,
	);
	register_post_type('video', $spareparts_args);
}
add_action( 'init', 'register_post_type_spareparts' );

add_action('add_meta_boxes', 'myplugin_add_custom_box');

function myplugin_add_custom_box(){
	$screens = array( 'video' );
	add_meta_box( 'myplugin_sectionid', 'Дополнительные поля', 'myplugin_meta_box_callback', $screens );
}

// HTML код блока
function myplugin_meta_box_callback( $post, $meta ){
	$screens = $meta['args'];

	// Используем nonce для верификации
	wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename' );

	// значение поля
	$value = get_post_meta( $post->ID, 'my_textfield', 1 );
	$order = get_post_meta( $post->ID, 'video_order', 1 );
	// Поля формы для введения данных
	echo '<label style="width: 105px; display: inline-block; margin-bottom: 5px;" for="my_textfield">Текстовое поле: </label> ';
	echo '<input type="text" style="margin-bottom: 10px;" id="my_textfield" name="my_textfield" value="'. $value .'" size="25" />';
	echo '<br><label style="width: 105px; display: inline-block; margin-bottom: 5px; margin-bottom: 10px;" for="video_order">Order: </label> ';
	echo '<input type="number" style="width: 50px; margin-bottom: 10px;" id="video_order" name="video_order" value="'. $order .'" />';
}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'myplugin_save_postdata' );
function myplugin_save_postdata( $post_id ) {
	// Убедимся что поле установлено.
	if ( ! isset( $_POST['my_textfield'] ) )
		return;
	if ( ! isset( $_POST['video_order'] ) )
		return;

	// проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
	if ( ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename(__FILE__) ) )
		return;

	// если это автосохранение ничего не делаем
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return;

	// проверяем права юзера
	if( ! current_user_can( 'edit_post', $post_id ) )
		return;

	// Все ОК. Теперь, нужно найти и сохранить данные
	// Очищаем значение поля input.
	$my_data = sanitize_text_field( $_POST['my_textfield'] );
	$my_data2 = sanitize_text_field( $_POST['video_order'] );
	

	// Обновляем данные в базе данных.
	update_post_meta( $post_id, 'my_textfield', $my_data );
	update_post_meta( $post_id, 'video_order', $my_data2 );
}
add_action( 'pre_get_posts', 'archive_orderby_meta'); 

    function archive_orderby_meta($query){
		if( !is_admin() && $query->is_archive('video')):           
		   $query->set( 'orderby', 'meta_value_num' );
		   $query->set( 'meta_query', array(
				'relation' => 'OR',
				array(
					'key' => 'video_order', 
					'compare' => 'NOT EXISTS'
				),
				array(
					'key' => 'video_order', 
					'compare' => 'EXISTS'
				)
        	));
		   $query->set( 'order', 'ASC' );
        endif;    
	};

?>
